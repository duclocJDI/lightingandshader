using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RotateCharacter : MonoBehaviour
{
    public Transform character;
    int state = 0;

    public void PressedRightBtn()
    {
        state = 1;
    }

    public void PressedLeftBtn()
    {
        state = 2;
    }

    private void Update()
    {
        if (state == 1)
        {
            character.localEulerAngles = new Vector3(character.localEulerAngles.x, character.localEulerAngles.y + 10f, character.localEulerAngles.z);
        }
        else if (state == 2)
        {
            character.localEulerAngles = new Vector3(character.localEulerAngles.x, character.localEulerAngles.y - 10f, character.localEulerAngles.z);
        }
    }

    public void Cancel()
    {
        state = 0;
        character.DOLocalRotate(new Vector3(0, 180, 0), 0.2f, RotateMode.Fast).SetEase(Ease.Linear);
        //float angle = Mathf.Abs(character.localEulerAngles.y) - 180;

        //if (360 - angle > angle)
        //{
        //    character.DOLocalRotate(new Vector3(0, 180, 0), 0.5f, RotateMode.Fast).SetEase(Ease.Linear);
        //}
        //else
        //{
        //    character.DOLocalRotate(new Vector3(0, 180, 0), 0.5f, RotateMode.Fast).SetEase(Ease.Linear);
        //}
    }
}
